"""
Date: July 20th 2019
Author: Willaim 
Purpose:
        This will be a recreation of 'Conway's Game Of Life'

        The Game Will follow the following rules:

            1. Any live cell with less than two live neighbours dies, as if by underpopulation
            2. any live cell with two or three live neighbours lives on to the next generation
            3. Any live cell with more than three live neighbours dies, as if by overpopulation
            4. any dead cell with three live neighbours becomes a live cell

        The Game will start by presenting a grid of white spaces to the player. Ther grid will be fairly large.
        If the player clicks on a square of the grid it will change tfrom white to black. If they click it again it will change back to white.
        when the player is ready they will press a button to stat the game. The game will tick through generations.



"""
import pygame
from pygame.locals import *
from pygame import Color, Rect, Surface
import time
import random

white = ( 255, 255, 255)
black = (0,0,0)
blue = (0,0,255)

red = (255,0,0)
yellow = (0,255,255)
gridSize = 1000
cellSize = 10

windowScale = 1100






class cell:
    def __init__(self, position, alive = False):
        self.alive = alive
        self.position = position 
    def die(self):
        self.alive = False
    def becomeAlive(self):
        self.alive = True
    def switch(self):
        if (self.alive):
            self.alive = False
        else:
            self.alive = True
            
def setup(screen, cellMap, listOfRectangles):
    #pygame.init()
    screen.fill(white)
    
    #create cells
    for i in range (int(gridSize/cellSize)):
        cellMap.append([])
        listOfRectangles.append([])
        for k in range (int(gridSize/cellSize)):
            cellMap[i].append(cell([i,k]))
            listOfRectangles[i].append(Rect((cellSize*i, cellSize*k), (cellSize, cellSize)))
            
    #if user clicks on a cell change that cells state such that if it is alive it becomes dead and if it is dead it becomes alive
    #and also set the cells color to the correct state
    selected_r_i = 0
    selected_r_k = 0
    while(True):
        ev = pygame.event.get()
        for event in ev:
            if (event.type == pygame.KEYDOWN):
                if (event.key == pygame.K_RETURN):
                    return #       this is the only way out of this loop. The user must press enter
            elif (event.type == pygame.MOUSEBUTTONDOWN):
                pos = pygame.mouse.get_pos()
                for i in range (len(listOfRectangles)):
                    breakout = False
                    for k in range (len(listOfRectangles[i])):
                        if ( listOfRectangles[i][k].collidepoint(pos) ):
                            selected_r_i = i
                            selected_r_k = k
                            breakout = True
                            break
                    if (breakout):
                        break
                               
                print("STUB(setup): ",selected_r_i," ",selected_r_k)
                cellMap[selected_r_i][selected_r_k].switch()

                #reset
                selected_r_i = 0
                selected_r_k = 0
        update_board(screen, cellMap, listOfRectangles)
                    

def update_board(screen, cellMap, listOfRectangles):
    screen.fill(white)#screen.fill((random.randrange(100,255), random.randrange(100,255), random.randrange(100,255))) #fill screen with random color (this visually signifies that the game is being updated)
    for i in range (len(cellMap)-1):
        for k in range (len(cellMap[i])-1):
            if (cellMap[i][k].alive == True):
                pygame.draw.rect(screen, black, listOfRectangles[i][k], 0)
            else:
                pygame.draw.rect(screen, white, listOfRectangles[i][k], 0)

            if (i == 0):
                pygame.draw.rect(screen, blue, listOfRectangles[i][k], 0)
            if (k == 0):
                pygame.draw.rect(screen, red, listOfRectangles[i][k], 0)
                
    pygame.display.flip()


class futureCell_map:
    def __init__(self, cellMap):
        print("here i am")
        self.map = []
        self.sizex = len(cellMap)-1
        self.sizey = len(cellMap[0])-1
        for i in range (self.sizex):
            self.map.append([])
            for k in range (self.sizey):
                self.map[i].append(cellMap[i][k].alive)
                
                    
    def update(newMap):
        for i in range (self.sizex):
            for k in range (self.sizey):
                if (newMap[i][k].alive):
                    self.map[i][k] = True
                else:
                    self.map[i][k] = False
        
    

def the_game_of_life(screen, cellMap, listOfRectangles):
    fcm = futureCell_map(cellMap)
    print("START---------------------------------------")
    while(True):
        #print("top loop------------------------")
        
        #time.sleep(0.5)
      
        for i in range (len(cellMap)-1):
            #print("next row ", i)
            for k in range (len(cellMap[i])-1):
                livingNeighbours = 0
                
                if (cellMap[i+1][k+1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("a")

                        
                if (cellMap[i-1][k-1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("b")

                        
                if (cellMap[i+1][k-1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("c")

                        
                if (cellMap[i-1][k+1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("d")


                        
            
                if (cellMap[i][k+1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("e")

                        
                if (cellMap[i][k-1].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("f")

                        
                if (cellMap[i+1][k].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("g")

                        
                if (cellMap[i-1][k].alive):
                    livingNeighbours+=1
                    
                    if (cellMap[i][k].alive):
                        print("h")



                if (livingNeighbours < 2):#Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                    fcm.map[i][k] = False
                    if (cellMap[i][k].alive):
                        print("cell (",i," , ",k," ) died of under population. neighbour count = ", livingNeighbours)

                        
                if (livingNeighbours == 2) or (livingNeighbours == 3):#Any live cell with two or three live neighbours lives on to the next generation.
                    print("cell (",i," , ",k," ) lives On. neighbour count = ", livingNeighbours)

                        
                if (livingNeighbours > 3):#Any live cell with more than three live neighbours dies, as if by overpopulation.
                    fcm.map[i][k] = False
                    print("cell (",i," , ",k," ) died of over population. neighbour count = ", livingNeighbours)

                    
                if (livingNeighbours == 3):#Any dead cell with three live neighbours becomes a live cell, as if by reproduction.
                    fcm.map[i][k] = True
                    if (cellMap[i][k].alive != True):
                        print("cell (",i," , ",k," ) is born! neighbour count = ", livingNeighbours)
                        

        print(" ")

        for i in range (len(cellMap)-1):
            for k in range (len(cellMap[i])-1):
                if (fcm.map[i][k] == True):
                    cellMap[i][k].alive = True
                else:
                    cellMap[i][k].alive = False
        
        update_board(screen, cellMap, listOfRectangles)
    
        
                
                
        
    
    
            


def main():
    #create windows from here. first window to set up the game.
    #then take that setup input and feed it to the life loop
    pygame.init()
    screen = pygame.display.set_mode((windowScale, windowScale))

    cellMap = []
    listOfRectangles=[]
    
    setup(screen, cellMap, listOfRectangles)
    the_game_of_life(screen, cellMap, listOfRectangles)




main()
